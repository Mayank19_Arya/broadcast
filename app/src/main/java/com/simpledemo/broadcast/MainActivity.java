package com.simpledemo.broadcast;

import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    Button camera;
    int Cap=1;
    Uri URI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        camera=(Button)findViewById(R.id.Camera_button);


 camera.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View v) {
        // Intent in=new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
         Intent in = new Intent("android.media.action.IMAGE_CAPTURE");
         //Intent in=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
         if (in.resolveActivity(getPackageManager()) != null) {
             File photoFile = null;
         try {
             photoFile = createImageFile();
         } catch (AbstractMethodError ex) {
             System.out.println("error");
         }
         if (photoFile != null){
             URI= FileProvider.getUriForFile(getApplicationContext(),"com.example.android.filedriver",photoFile);
             in.putExtra(MediaStore.EXTRA_OUTPUT, URI);
         }
             startActivityForResult(in, Cap);
     }
 }
});


    }
    protected File createImageFile() {
        File imagename=null;
        String time = new SimpleDateFormat("ddmmyyyy_HHmmss").format(new Date());
        String name = "JPEG_" + time + "_";
        File Dir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        try {
            imagename = File.createTempFile(name, ".jpg", Dir);
        } catch (IOException i) {
            System.out.println("error");
        }
            return imagename;
        }



    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
if(requestCode==Cap){
    Bitmap image=(Bitmap)data.getExtras().get("data");
    ImageView img=(ImageView)findViewById(R.id.imageView);
    img.setImageBitmap(image);
}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
